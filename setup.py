import setuptools

setuptools.setup(name="bla",
                 version_format='{tag}',  # .dev{commitcount}+{gitsha}',
                 author="Torsten Rendler",
                 author_email="t.rendler@micro-biolytics.com",
                 packages=setuptools.find_packages(),
                 setup_requires=['setuptools-git-ver'],)
